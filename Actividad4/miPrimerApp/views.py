from django.shortcuts import render, HttpResponse
from .models import *

# Create your views here.
def index(request):
    varEstudiante = Estudiante.objects.filter(grupo= 1)
    varEstudianteG2 = Estudiante.objects.filter(grupo= 2)
    varEstudianteG3 = Estudiante.objects.filter(grupo= 3)
    varEstudianteG4 = Estudiante.objects.filter(grupo= 4)
    varEdad = Estudiante.objects.filter(edad= 22)
    varApellido = Estudiante.objects.filter(apellidos = "Mendoza")
    varGrupoApellido = Estudiante.objects.filter(grupo= 3 ,apellidos = "Mendoza")
    
  
    return render(request, 'index.html',{'varEstudiante':varEstudiante,'varEstudianteG2':varEstudianteG2,'varEstudianteG3':varEstudianteG3,'varEstudianteG4':varEstudianteG4, 'varEdad':varEdad ,'varApellido': varApellido, 'varGrupoApellido':varGrupoApellido})

